package ictgradschool.industry.lab13.ex01;

public class ExerciseOne {
    public static void main(String[] args) {
        Runnable myRunnerable = new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()){
                    for (int i = 0; i < 1000000; i++) {
                        if (Thread.currentThread().isInterrupted())
                            break;
                        System.out.println(i);
                    }
                }
            }
        };

        Thread myThread = new Thread(myRunnerable);

        myThread.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        myThread.interrupt();
    }
}
