package ictgradschool.industry.lab13.ex03;

import ictgradschool.Keyboard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    private final int NUM_THREAD = 10;
    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    private long insideSamples = 0;

    @Override
    protected void start() throws InterruptedException {

        System.out.println("Enter the number of samples to use for approximation:");
        System.out.print("> ");

        long numSamples = Long.parseLong(Keyboard.readInput());

        System.out.println("Estimating PI...");

        Instant startTime = Instant.now();

        // Do the estimation
        double estimatedPi = this.estimateMultiPI(numSamples);

        Instant endTime = Instant.now();
        Duration timeInMillis = Duration.between(startTime, endTime);

        double difference = Math.abs(estimatedPi - Math.PI);
        double differencePercent = 100.0 * difference / Math.PI;
        NumberFormat format = new DecimalFormat("#.####");

        System.out.println("Estimate of PI: " + estimatedPi);
        System.out.println("Estimate is within " + format.format(differencePercent) + "% of Math.PI");
        System.out.println("Estimation took " + timeInMillis + " milliseconds");

    }

    /**
     * Very inefficient lock
     */
    private synchronized void incrementSample(){
        this.insideSamples++;
    }

    /**
     * Better lock
     */
    private synchronized void addInsideSample(long in){
        insideSamples += in;
    }


    protected double estimateMultiPI(long numSamples) throws InterruptedException {
        // TODO Implement this.

        List<Thread> calcs = new ArrayList<>();
        for (int i = 0; i < NUM_THREAD; ++i){
            calcs.add(new Thread(() -> {
                long inCircleSample = 0;
                ThreadLocalRandom tlr = ThreadLocalRandom.current();
                for (long i1 = 0; i1 < numSamples/NUM_THREAD; i1++) {

                    double x = tlr.nextDouble();
                    double y = tlr.nextDouble();

                    if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                        inCircleSample++;
                    }
                }
                addInsideSample(inCircleSample);
            }));
        }

        for (Thread calc : calcs) {

            calc.start();
        }

        for (Thread calc : calcs) {
            try {
                calc.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        return 4.0 * (double) insideSamples / (double) numSamples;
    }

    /** Program entry point. */
    public static void main(String[] args) throws InterruptedException {
        new ExerciseThreeMultiThreaded().start();
    }
}
